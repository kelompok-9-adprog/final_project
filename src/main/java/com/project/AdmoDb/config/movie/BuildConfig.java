package com.project.AdmoDb.config.movie;

import org.springframework.context.annotation.Configuration;

@Configuration
public class BuildConfig {
    public static final String API_KEY = "a5fc87dadba150d80b50f923b97fe181";
    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    public static final String LANGUAGE = "EN-US";
}
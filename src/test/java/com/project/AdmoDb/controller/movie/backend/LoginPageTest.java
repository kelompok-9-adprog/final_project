/**
 * Created by Dawid Stankiewicz on 25.07.2016
 */
package com.project.AdmoDb.controller.movie.backend;

import com.project.AdmoDb.controller.backend.UserController;
import com.project.AdmoDb.entity.movie.MovieRepository;
import com.project.AdmoDb.service.UserService;
import com.project.AdmoDb.service.model.UserProfileService;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = UserController.class)
public class LoginPageTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @MockBean
    private UserProfileService userProfileService;

    @MockBean
    private MovieRepository movieRepository;

    @Test
    @Ignore
    public void testLogin() throws Exception {

        this.mockMvc
                .perform(post("/login")
                        .param("username", "admin")
                        .param("password", "password"))
                .andExpect(status()
                        .is3xxRedirection())
                .andExpect(redirectedUrl("/"));
    }

}

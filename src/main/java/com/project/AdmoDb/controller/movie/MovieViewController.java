package com.project.AdmoDb.controller.movie;

import com.project.AdmoDb.entity.backend.UserProfile;
import com.project.AdmoDb.entity.backend.User;
import com.project.AdmoDb.exception.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.project.AdmoDb.service.model.UserProfileService;

@Controller
public class MovieViewController {

    @Autowired
    private UserProfileService userProfileService;

    @GetMapping("/movielist")
    public String getMoviesByGenre(@RequestParam(name = "genre") String genre, @RequestParam(name = "page") String page) {
        return "movies";
    }

    @GetMapping("/movie")
    public String getMovieById(@RequestParam(name = "id") String id, Authentication authentication, Model model) {

        if(authentication == null) return "signin";

        String username = authentication.getName();
        UserProfile userProfile;
        try {
            userProfile = userProfileService.findOne(username);
        } catch (NullPointerException e) {
            throw new UserNotFoundException();
        }
        User user = userProfile.getUser();
        int userid = user.getId();
        model.addAttribute("userid", userid);
        return "movie-details";
    }

    @GetMapping("/favoritemovie")
    public String getLikedMoviesByUserId(Authentication authentication, Model model) {
        String username = authentication.getName();
        UserProfile userProfile;
        try {
            userProfile = userProfileService.findOne(username);
        } catch (NullPointerException e) {
            throw new UserNotFoundException();
        }
        User user = userProfile.getUser();
        int userid = user.getId();
        model.addAttribute("userid", userid);
        return "favoritemovie";
    }
}

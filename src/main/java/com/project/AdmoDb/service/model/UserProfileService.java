package com.project.AdmoDb.service.model;

import com.project.AdmoDb.entity.backend.UserProfile;


public interface UserProfileService {

    public UserProfile findOne(int userId);

    public UserProfile findOne(String username);

}

/**
 * Created by Dawid Stankiewicz on 17.07.2016
 */
package com.project.AdmoDb.entity.backend.repository;

import com.project.AdmoDb.entity.backend.Section;
import org.springframework.data.jpa.repository.JpaRepository;


public interface SectionRepository extends JpaRepository<Section, Integer> {

    Section findByName(String name);

    Section findById(int id);

}

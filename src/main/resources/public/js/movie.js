var genreIds = new Array();
var genreNames = new Array();
var genreArray = new Array();
var movieGenreIds = new Array();
likedMovieExist = false;

$(document).ready(function(){

    fillGenreArray();
    getGenreSidebar();
    getSingleMovieResult();

    function getURLParameter(param){
        var pageURL = window.location.search.substring(1);
        var URLVariables = pageURL.split('&');
        for (var i = 0; i < URLVariables.length; i++)
        {
            var parameterName = URLVariables[i].split('=');
            if (parameterName[0] == param)
            {
                return parameterName[1];
            }
        }
    }

    function getGenreSidebar(){
        $(function() {
            $.ajax({
                type: 'GET',
                url: 'getgenrelist',
                dataType: 'json',
                success: function(result){
                    for (var i = 0; i < result.genres.length; i++) {
                        $('<li><a href="/movielist?genre=' + result.genres[i].id + '&page=1" class="d-flex">' +
                        result.genres[i].name + '</a>'
                        ).appendTo('#rightSidebar');
                    }
                }
            });
        });
    }

    function getSingleMovieResult() {
        $(function() {
            var movieId = getURLParameter('id');
            var userId = $('#movie').attr('value');
            $.ajax({
                type: 'GET',
                url: 'getmovie?id=' + movieId,
                dataType: 'json',
                success: function(movie){
                    $.each(movie.genres, function(i, genre) {
                        movieGenreIds.push(genre.id);
                    });
                    var thisGenre = getMovieGenreName(movieGenreIds);
                    var singlePost = $('<div class="single-post">');
                    var blogDetails = $('<div class="blog_details">');
                    var blogDetailsUl = $('<ul class="blog-info-link mt-3 mb-4">');
                    $('<div class="feature-img"><img class="img-fluid" src="http://image.tmdb.org/t/p/w185/' +
                    movie.poster_path + '" style="width: 35% ; height: 35%"></div>').appendTo(singlePost);
                    $('<h2>' + movie.title + '</h2>').appendTo(blogDetails);
                    blogDetailsUl.appendTo(blogDetails);
                    $('<p>Genre: ' + thisGenre + '</p>').appendTo(blogDetailsUl);
                    $('<p>Rating: ' + movie.vote_average + '</p>').appendTo(blogDetailsUl);
                    $('<p>Release date: ' + movie.release_date + '</p>').appendTo(blogDetailsUl);
                    $('<p>Runtime: ' + movie.runtime + ' minutes</p>').appendTo(blogDetailsUl);
                    $('<p>Status: ' + movie.status + '</p>').appendTo(blogDetailsUl);
                    $('<p>Summary:</p>').appendTo(blogDetailsUl);
                    $('<p>' + movie.overview + '</p>').appendTo(blogDetailsUl);
                    blogDetails.appendTo(singlePost);
                    singlePost.appendTo('#movieDetail');
                    checkLikedMovie(movieId, userId);
                }
            });
        });
    }

    function likeMovie(object, userId) {
        if (object.hasClass("fa-heart-o")) {
            saveLikedMovie(object.attr('value'), userId);
            object.removeClass("fa-heart-o").addClass("fa-heart").addClass("red-clr");
        } else {
            deleteLikedMovie(object.attr('value'), userId);
            object.removeClass("fa-heart").removeClass("red-clr").addClass("fa-heart-o");
        }
    }

    $("#like").delegate("#likebtn", "click", function() {
        likeMovie($(this), $('#movie').attr('value'));
    });

    function saveLikedMovie(movieId, userId){
        $(function(){
            $.ajax({
                type: 'GET',
                url: "savelikedmovie?movieid=" + movieId + "&userid=" + userId,
            });
        });
    }

    function deleteLikedMovie(movieId, userId) {
        $(function(){
            $.ajax({
                type: 'GET',
                url: "deletelikedmovie?movieid=" + movieId + "&userid=" + userId,
            });
        });
    }

    function checkLikedMovie(movieId, userId) {
        $(function() {
            $.ajax({
                type: 'GET',
                url: "getlikedmovie?userid=" + userId,
                dataType: 'json',
                success: function(result){
                    $.each(result.likedMovies, function(i, likedMovie) {
                        if (likedMovie.movieId == movieId) {
                            likedMovieExist = true;
                        }
                    });
                    if (likedMovieExist == false) {
                        $('<div id="likebtn" class="fa fa-heart-o" style="font-size:40px" value="' + movieId + '"></div>').appendTo('#like');
                        $('<h4 class="widget_title">Add to Favourite Movies!</h4>').appendTo('#like');
                    } else if (likedMovieExist == true) {
                        $('<div id="likebtn" class="fa fa-heart red-clr" style="font-size:24px" value="' + movieId + '"></div>').appendTo('#like');
                        $('<h4 class="widget_title">Add to Favourite Movies!</h4>').appendTo('#like');
                    }
                }
            });
        });
    }

    function fillGenreArray() {
        $(function() {
            $.ajax({
                type: 'GET',
                url: 'getgenrelist',
                dataType: 'json',
                success: function(result){
                    $.each(result.genres, function(i, genre) {
                        var genrePair = new Array();
                        genreIds[i] = genre.id;
                        genreNames[i] = genre.name;
                        genrePair[0] = genreIds[i];
                        genrePair[1] = genreNames[i];
                        genreArray.push(genrePair);
                    });
                }
            });
        });
    }

    function getMovieGenreName(movieGenreId) {
        var movieGenreStr = "";

        for (i = 0; i < movieGenreId.length; i++) {
            for (j = 0; j < genreArray.length; j++) {
                if (genreArray[j][0] == movieGenreId[i]) {
                    movieGenreStr += genreArray[j][1];
                }
            }
            if (i != movieGenreId.length - 1) {
                movieGenreStr += ", ";
            }
        }
        return movieGenreStr;
    }

});
/**
 * Created by Dawid Stankiewicz on 18.07.2016
 */
package com.project.AdmoDb.entity.backend.repository;

import com.project.AdmoDb.entity.backend.Section;
import com.project.AdmoDb.entity.backend.Topic;
import com.project.AdmoDb.entity.backend.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;


public interface TopicRepository extends JpaRepository<Topic, Integer> {

    Set<Topic> findBySection(Section section);

    Set<Topic> findByUser(User user);

    Set<Topic> findAllByOrderByCreationDateDesc();

    Set<Topic> findTop5ByOrderByCreationDateDesc();

    Topic findById(int id);


}

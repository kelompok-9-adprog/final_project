var genreIds = new Array();
var genreNames = new Array();
var genreArray = new Array();

$(document).ready(function(){

    fillGenreArray();
    getMovieResult();
    getGenreSidebar();

    function getURLParameter(param){
        var pageURL = window.location.search.substring(1);
        var URLVariables = pageURL.split('&');
        for (var i = 0; i < URLVariables.length; i++)
        {
            var parameterName = URLVariables[i].split('=');
            if (parameterName[0] == param)
            {
                return parameterName[1];
            }
        }
    }

    function getGenreSidebar(){
        $(function() {
            $.ajax({
                type: 'GET',
                url: 'getgenrelist',
                dataType: 'json',
                success: function(result){
                    for (var i = 0; i < result.genres.length; i++) {
                        $('<li><a href="/movielist?genre=' + result.genres[i].id + '&page=1" class="d-flex">' +
                        result.genres[i].name + '</a>'
                        ).appendTo('#rightSidebar');
                    }
                }
            });
        });
    }

    function getMovieResult() {
        $(function() {
            var movieGenre = getURLParameter('genre');
            var resultPage = getURLParameter('page');
            $.ajax({
                type: 'GET',
                url: 'getmovielist?genre=' + movieGenre + '&page=' + resultPage,
                dataType: 'json',
                success: function(result){
                    $.each(result.results, function(i, movie) {
                        var thisGenre = getMovieGenreName(movie.genre_ids);
                        var blog_item = $('<article class="blog_item">');
                        var blog_item_img = $('<div class="blog_item_img">');
                        var blog_details = $('<div class="blog_details">');
                        $('<img src="http://image.tmdb.org/t/p/w185/' +
                        movie.poster_path + '" style="width: 35% ; height: 35%"><a class="blog_item_date"><h3><i class="fa fa-star"' +
                        ' style="font-size:42px;color:white"></i>Rating: ' + movie.vote_average + '</h3></a>').appendTo(blog_item_img);
                        $('<a class="d-inline-block"><h2>' + movie.title + '</h2></a>').appendTo(blog_details);
                        $('<p>Genre: ' + thisGenre +'</p>').appendTo(blog_details);
                        $('<p>Release Date: ' + movie.release_date + '</p>').appendTo(blog_details);
                        $('<p>Summary:</p>').appendTo(blog_details);
                        $('<p>' + movie.overview + '</p>').appendTo(blog_details);
                        $('<div class="input-group-append align-items-center text-center"' +
                        ' style="align-items:center ; dislay:flex ; flex-direction:row ; justify-content:center">' +
                        '<a href = "/movie?id=' + movie.id + '"><button class="button">View Movie</button></a></div>').appendTo(blog_details);
                        blog_item_img.appendTo(blog_item);
                        blog_details.appendTo(blog_item);
                        blog_item.appendTo('#leftSidebar');
                    });
                }
            });
            for (i = 0; i < 15; i++) {
                $('<li class="page-item"><a class="page-link" href="/movielist?genre=' + movieGenre + '&page=' +
                (i+1) + '">' + (i+1) + '</a></li>').appendTo('#pagebutton');
            }
        });
    }

    function fillGenreArray() {
        $(function() {
            $.ajax({
                type: 'GET',
                url: 'getgenrelist',
                dataType: 'json',
                success: function(result){
                    $.each(result.genres, function(i, genre) {
                        var genrePair = new Array();
                        genreIds[i] = genre.id;
                        genreNames[i] = genre.name;
                        genrePair[0] = genreIds[i];
                        genrePair[1] = genreNames[i];
                        genreArray.push(genrePair);
                    });
                }
            });
        });
    }

    function getMovieGenreName(movieGenreId) {
        var movieGenreStr = "";
        for (i = 0; i < movieGenreId.length; i++) {
            for (j = 0; j < genreArray.length; j++) {
                if (genreArray[j][0] == movieGenreId[i]) {
                    movieGenreStr += genreArray[j][1];
                }
            }
            if (i != movieGenreId.length - 1) {
                movieGenreStr += ", ";
            }
        }
        return movieGenreStr;
    }

});
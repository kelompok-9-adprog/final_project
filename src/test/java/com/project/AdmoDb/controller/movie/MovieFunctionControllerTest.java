package com.project.AdmoDb.controller.movie;

import com.project.AdmoDb.entity.movie.MovieRepository;
import com.project.AdmoDb.service.UserService;
import com.project.AdmoDb.service.model.UserProfileService;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = MovieFunctionController.class)
public class MovieFunctionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MovieRepository movieRepository;

    @MockBean
    private UserService userService;

    @MockBean
    private UserProfileService userProfileService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void getGenreList() throws Exception {
        this.mockMvc.perform(get("/getgenrelist").accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.genres", Matchers.hasSize(19)))
                .andExpect(jsonPath("$.genres[0].id").value(28))
                .andExpect(jsonPath("$.genres[0].name").value("Action"));
    }

    @Test
    public void getMovieList() throws Exception {
        this.mockMvc.perform(get("/getmovielist?genre=28&page=1").accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.results", Matchers.hasSize(20)));
    }

    @Test
    public void getSingleMovieList() throws Exception {
        this.mockMvc.perform(get("/getmovie?id=447404").accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.original_title").value("Pokémon Detective Pikachu"));
    }

    @Test
    public void saveLikedMovies() throws Exception {
        this.mockMvc.perform(get("/savelikedmovie?movieid=447404&userid=123").accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.liked").value("447404 123"));
    }

    @Test
    public void deleteLikedMovies() throws Exception {
        this.mockMvc.perform(get("/deletelikedmovie?movieid=447404&userid=123").accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.deleted").value("447404 123"));
    }

    @Test
    public void getLikedMovies() throws Exception {
        this.mockMvc.perform(get("/getlikedmovie?userid=123").accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.likedMovies", Matchers.hasSize(0)));
    }
}

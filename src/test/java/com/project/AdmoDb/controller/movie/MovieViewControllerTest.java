package com.project.AdmoDb.controller.movie;

import com.project.AdmoDb.entity.movie.MovieRepository;
import com.project.AdmoDb.service.UserService;
import com.project.AdmoDb.service.model.UserProfileService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = MovieViewController.class)
public class MovieViewControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MovieRepository movieRepository;

    @MockBean
    private UserService userService;

    @MockBean
    private UserProfileService userProfileService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void getMovieAdventure() throws Exception {
        this.mockMvc.perform(get("/movielist?genre=12&page=1"))
                .andExpect(view().name("movies"));
    }

}

package com.project.AdmoDb.controller.movie;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.project.AdmoDb.config.movie.BuildConfig;
import com.project.AdmoDb.entity.movie.LikedMovie;
import com.project.AdmoDb.entity.movie.LikedMovieResponse;
import com.project.AdmoDb.entity.movie.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
public class MovieFunctionController {
    @Autowired
    MovieRepository repository;

    @RequestMapping(value = "/getmovielist", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getMovieList(@RequestParam(name = "genre") String genre,
                                               @RequestParam(name = "page") String page) {
        RestTemplate template = new RestTemplate();
        ResponseEntity<String> response = template.getForEntity(BuildConfig.BASE_URL + "discover/movie?with_genres="
                + genre + "&page=" + page + "&sort_by=popularity.desc&language=" + BuildConfig.LANGUAGE + "&api_key=" +
                BuildConfig.API_KEY, String.class);
        return response;
    }

    @RequestMapping(value = "/getgenrelist", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getGenreList() {
        RestTemplate template = new RestTemplate();
        ResponseEntity<String> response = template.getForEntity(BuildConfig.BASE_URL + "genre/movie/list?language="
                + BuildConfig.LANGUAGE + "&api_key=" + BuildConfig.API_KEY, String.class);
        return response;
    }

    @RequestMapping(value = "/getmovie", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getMovie(@RequestParam(name = "id") String id) {
        RestTemplate template = new RestTemplate();
        ResponseEntity<String> response = template.getForEntity(BuildConfig.BASE_URL + "movie/" + id + "?language="
                + BuildConfig.LANGUAGE + "&api_key=" + BuildConfig.API_KEY, String.class);
        return response;
    }

    @RequestMapping(value = "/savelikedmovie")
    public String saveLikedMovie(@RequestParam(name = "movieid") String movieId, @RequestParam(name = "userid") int userId) {
        LikedMovie likedMovie = new LikedMovie(movieId, userId);
        repository.save(likedMovie);
        Gson gson = new Gson();
        JsonObject json = new JsonObject();
        json.addProperty("liked", (movieId + " " + Integer.toString(userId)));
        return gson.toJson(json);
    }

    @RequestMapping(value = "/deletelikedmovie")
    public String deleteLikedMovie(@RequestParam(name = "movieid") String movieId, @RequestParam(name = "userid") int userId) {
        LikedMovie movie = repository.findByMovieIdAndUserId(movieId, userId);
        repository.delete(movie);
        Gson gson = new Gson();
        JsonObject json = new JsonObject();
        json.addProperty("deleted", (movieId + " " + Integer.toString(userId)));
        return gson.toJson(json);
    }

    @RequestMapping(value = "/getlikedmovie", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getLikedMovie(@RequestParam(name = "userid") int userId) {
        Gson gson = new Gson();
        List<LikedMovie> likedMovies = repository.findByUserId(userId);
        LikedMovieResponse response = new LikedMovieResponse(likedMovies);
        return gson.toJson(response);
    }
}

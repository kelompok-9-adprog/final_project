/**
 * Created by Dawid Stankiewicz on 17.07.2016
 */
package com.project.AdmoDb.service;

import com.project.AdmoDb.entity.backend.Section;
import com.project.AdmoDb.entity.backend.Topic;
import com.project.AdmoDb.entity.backend.User;

import java.util.List;
import java.util.Set;


public interface TopicService {

    List<Topic> findAll();

    Topic findOne(int id);

    Set<Topic> findRecent();

    Set<Topic> findAllByOrderByCreationDateDesc();

    Set<Topic> findBySection(Section section);

    Set<Topic> findBySection(String sectionName);

    Topic save(Topic topic);

    Set<Topic> findBySection(int id);

    Set<Topic> findByUser(User user);

    void delete(int id);

    void delete(Topic topic);

}

var genreIds = new Array();
var genreNames = new Array();
var genreArray = new Array();
var movieGenreIds = new Array();

$(document).ready(function(){

    fillGenreArray();
    getGenreSidebar();
    getLikedMovieResult();

    function getGenreSidebar(){
        $(function() {
            $.ajax({
                type: 'GET',
                url: 'getgenrelist',
                dataType: 'json',
                success: function(result){
                    for (var i = 0; i < result.genres.length; i++) {
                        $('<li><a href="/movielist?genre=' + result.genres[i].id + '&page=1" class="d-flex">' +
                        result.genres[i].name + '</a>'
                        ).appendTo('#rightSidebar');
                    }
                }
            });
        });
    }

    function getLikedMovieResult() {
        $(function() {
            var userId = $('#movie').attr('value');
            $.ajax({
                type: 'GET',
                url: 'getlikedmovie?userid=' + userId,
                dataType: 'json',
                success: function(result){
                    if (result.likedMovies.length != 0) {
                        $.each(result.likedMovies, function(i, likedMovie) {
                            getIndividualLikedMovie(likedMovie.movieId);
                        });
                    } else {
                        $('<h4>').text("You have not liked any movie").appendTo('#leftSidebar');
                    }
                }
            });
        });
    }

    function getIndividualLikedMovie(movieId) {
        $(function() {
            $.ajax({
                type: 'GET',
                url: 'getmovie?id=' + movieId,
                dataType: 'json',
                success: function(movie){
                    $.each(movie.genres, function(i, genre) {
                        movieGenreIds.push(genre.id);
                    });
                    var thisGenre = getMovieGenreName(movieGenreIds);
                    var blog_item = $('<article class="blog_item">');
                    var blog_item_img = $('<div class="blog_item_img">');
                    var blog_details = $('<div class="blog_details">');
                    $('<img src="http://image.tmdb.org/t/p/w185/' +
                    movie.poster_path + '" style="width: 35% ; height: 35%"><a class="blog_item_date"><h3><i class="fa fa-star"' +
                    ' style="font-size:42px;color:white"></i>Rating: ' + movie.vote_average + '</h3></a>').appendTo(blog_item_img);
                    $('<a class="d-inline-block"><h2>' + movie.title + '</h2></a>').appendTo(blog_details);
                    $('<p>Genre: ' + thisGenre +'</p>').appendTo(blog_details);
                    $('<p>Release Date: ' + movie.release_date + '</p>').appendTo(blog_details);
                    $('<p>Summary:</p>').appendTo(blog_details);
                    $('<p>' + movie.overview + '</p>').appendTo(blog_details);
                    $('<div class="input-group-append align-items-center text-center"' +
                    ' style="align-items:center ; dislay:flex ; flex-direction:row ; justify-content:center">' +
                    '<a href = "/movie?id=' + movie.id + '"><button class="button">View Movie</button></a></div>').appendTo(blog_details);
                    blog_item_img.appendTo(blog_item);
                    blog_details.appendTo(blog_item);
                    blog_item.appendTo('#leftSidebar');
                }
            });
        });
    }

    function fillGenreArray() {
        $(function() {
            $.ajax({
                type: 'GET',
                url: 'getgenrelist',
                dataType: 'json',
                success: function(result){
                    $.each(result.genres, function(i, genre) {
                        var genrePair = new Array();
                        genreIds[i] = genre.id;
                        genreNames[i] = genre.name;
                        genrePair[0] = genreIds[i];
                        genrePair[1] = genreNames[i];
                        genreArray.push(genrePair);
                    });
                }
            });
        });
    }

    function getMovieGenreName(movieGenreId) {
        var movieGenreStr = "";
        for (i = 0; i < movieGenreId.length; i++) {
            for (j = 0; j < genreArray.length; j++) {
                if (genreArray[j][0] == movieGenreId[i]) {
                    movieGenreStr += genreArray[j][1];
                }
            }
            if (i != movieGenreId.length - 1) {
                movieGenreStr += ", ";
            }
        }
        return movieGenreStr;
    }

});
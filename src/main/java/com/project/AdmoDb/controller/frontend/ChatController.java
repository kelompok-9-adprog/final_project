package com.project.AdmoDb.controller.frontend;

import com.project.AdmoDb.entity.backend.User;
import com.project.AdmoDb.entity.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ChatController {

    @Autowired
    UserRepository repo;

    @GetMapping("/chat")
    public String chat(@RequestParam(name = "chat", required = false)
                                   String name, Model model, Authentication authentication) {

        String username = authentication.getName();
        User user = repo.findByUsername(username);
        model.addAttribute("user", user);
        model.addAttribute("chat", name);
        return "chat";
    }

    @ResponseBody
    @GetMapping("/get-username")
    public String getUsername(@RequestParam(name = "id") int id) {
        User user = repo.findById(id);
        if (user == null) {
            return "undefined";
        }
        return user.getUsername();
    }
}

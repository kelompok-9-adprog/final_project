/**
 * Created by Dawid Stankiewicz on 22.07.2016
 */
package com.project.AdmoDb.entity.backend.repository;

import com.project.AdmoDb.entity.backend.Role;
import org.springframework.data.jpa.repository.JpaRepository;


public interface RoleRepository extends JpaRepository<Role, Integer> {

    Role findByName(String name);

    Role findById(int id);

}

package com.project.AdmoDb.entity.movie;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "liked", schema = "ap_db")
public class LikedMovie {

    @Id
    @Column(name = "movieid")
    private String movieId;

    @Column(name = "userid")
    private int userId;

    public int getUserId() {
        return userId;
    }

    public String getMovieId() {
        return movieId;
    }

    public LikedMovie() {
    }

    public LikedMovie(String movieId, int userId) {

        this.movieId = movieId;
        this.userId = userId;
    }

}

var stompClient = null;

function connect() {
    var socket = new SockJS('http://admodb-chat.herokuapp.com/chat');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function(frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/messages'
            , function(messageOutput) {
                showMessageOutput(JSON.parse(messageOutput.body));
            });
    });
}

function sendMessage() {
    var fromId = document.getElementById('fromId').innerText;
    var text = document.getElementById('text').value;
    stompClient.send("/app/chat", {},
        JSON.stringify({'fromId': fromId, 'text':text}));
}

function showMessageOutput(messageOutput) {
    var response = document.getElementsByClassName('msg_history')[0];
    var child = document.createElement('div');
    console.log("id = " + document.getElementById('fromId').innerText + " - id = " + messageOutput.fromUserId);

    var username = "ancul";
    var Http = new XMLHttpRequest();
    Http.onreadystatechange = function() {
        if (Http.readyState == 4 && Http.status == 200) {
            username = Http.responseText;
        }

        if (messageOutput.fromUserId == parseInt(document.getElementById('fromId').innerText)) {
            child.className = "outgoing_msg";
            child.innerHTML = "<div class=\"sent_msg\">\n" +
                "                <p>" + messageOutput.chat + "</p>\n" +
                "                <span class=\"time_date\"> " + messageOutput.timestamp +"</span>\n" +
                "               </div>\n"
        } else {
            child.className = "incoming_msg";
            child.innerHTML =
                "              <div class=\"incoming_msg_img\"> <img src=\"https://ptetutorials.com/images/user-profile.png\" alt=\"sunil\"> </div>\n" +
                "              <div class=\"received_msg\">\n" +
                "                <span>" + username + "</span><div class=\"received_withd_msg\">\n" +
                "                  <p>" + messageOutput.chat + "</p>\n" +
                "                  <span class=\"time_date\">" + messageOutput.timestamp + "</span></div>\n" +
                "              </div>\n";
        }
        response.appendChild(child);

        $('.msg_history').animate({
            scrollTop: $('.msg_history').get(0).scrollHeight
        }, "fast");
    };
    Http.open("GET", '/get-username?id=' + messageOutput.fromUserId, false);
    Http.send();
}

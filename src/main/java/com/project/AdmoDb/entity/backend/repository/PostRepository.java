/**
 * Created by Dawid Stankiewicz on 18.07.2016
 */
package com.project.AdmoDb.entity.backend.repository;

import com.project.AdmoDb.entity.backend.Post;
import com.project.AdmoDb.entity.backend.Topic;
import com.project.AdmoDb.entity.backend.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;


public interface PostRepository extends JpaRepository<Post, Integer> {

    Set<Post> findByUser(User user);

    Set<Post> findByTopic(Topic topic);

    Set<Post> findAllByOrderByCreationDateDesc();

    Set<Post> findTop5ByOrderByCreationDateDesc();

    Post findById(int id);
}

/**
 * Created by Dawid Stankiewicz on 18.07.2016
 */
package com.project.AdmoDb.service.impl;

import com.project.AdmoDb.entity.backend.Section;
import com.project.AdmoDb.entity.backend.repository.SectionRepository;
import com.project.AdmoDb.service.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SectionServiceImpl implements SectionService {

    @Autowired
    private SectionRepository sectionRepository;

    @Override
    public List<Section> findAll() {
        return sectionRepository.findAll();
    }

    @Override
    public Section findOne(int id) {
        return sectionRepository.findById(id);
    }

    @Override
    public Section findByName(String name) {
        return sectionRepository.findByName(name);
    }

    @Override
    public Section save(Section section) {
        return sectionRepository.save(section);
    }

    @Override
    public void delete(int id) {
        delete(findOne(id));
    }

    @Override
    public void delete(Section section) {
        sectionRepository.delete(section);
    }

}

$(document).ready(function(){
    showGenres();

    function showGenres(){
        $(function() {
            $.ajax({
                type: 'GET',
                url: 'getgenrelist',
                dataType: 'json',
                success: function(result){
                    for (var i = 0; i < result.genres.length; i++) {
                        var currentRow;
                        if (i % 5 == 0) {
                            currentRow = $('<ul class="domain-check">');
                            currentRow.appendTo('#searchAndGenre');
                        }
                        $('<li class="checkbox_style">').append(
                            '<a class="button button-outline button-small" href="/movielist?genre=' +
                            result.genres[i].id + '&page=1">' + result.genres[i].name + '</a>'
                        ).appendTo(currentRow);
                    }
                }
            });
        });
    }
});
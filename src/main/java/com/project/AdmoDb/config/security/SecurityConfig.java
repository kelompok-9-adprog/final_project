package com.project.AdmoDb.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.web.filter.CharacterEncodingFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String[] FOR_AUTHORIZED_USERS = {"/user/**",
            "/profile/**",
            "/chat/**",
            "/favoritemovie"
    };

    private static final String[] FOR_ADMINS = {"/admin/**",
            "/users/**",
            "/section/new"};

    private static final String[] AUTHORIZED_ROLES = {"USER",
            "ADMIN"};

    private static final String[] ADMINS_ROLES = {"HEAD_ADMIN",
            "ADMIN"};

    private static final String[] PUBLIC_MATCHERS = {
            "/css/**",
            "/img/**",
            "/js/**",
            "/scss/**",
            "/vendors/**"
    };

    @Lazy
    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Lazy
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(FOR_AUTHORIZED_USERS).authenticated()
                .antMatchers(FOR_ADMINS).hasAnyAuthority(ADMINS_ROLES)
                .antMatchers("/registration").permitAll()
                .antMatchers(PUBLIC_MATCHERS).permitAll()
                .and()

                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()

                .logout()
                .permitAll()
                .and()

                .rememberMe()
                .tokenValiditySeconds(2419200)
                .key("forum-key");

        CharacterEncodingFilter filter = new CharacterEncodingFilter();
        filter.setEncoding("UTF-8");
        filter.setForceEncoding(true);
        http.addFilterBefore(filter, CsrfFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder);
    }

}

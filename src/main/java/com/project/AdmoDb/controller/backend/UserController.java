package com.project.AdmoDb.controller.backend;

import com.project.AdmoDb.controller.backend.form.NewUserForm;
import com.project.AdmoDb.entity.backend.UserProfile;
import com.project.AdmoDb.entity.backend.User;
import com.project.AdmoDb.exception.UserNotFoundException;
import com.project.AdmoDb.service.UserService;
import com.project.AdmoDb.service.model.UserProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
public class UserController {

    @Lazy
    @Autowired
    private UserService userService;

    @Lazy
    @Autowired
    private UserProfileService userProfileService;

    @GetMapping("/user/{username}")
    public String findUserByUsernameAndViewProfilePage(@PathVariable String username,
                                                       Model model) {
        UserProfile userProfile;
        try {

            userProfile = userProfileService.findOne(username);
        } catch (NullPointerException e) {
            throw new UserNotFoundException();
        }
        model.addAttribute("userProfile", userProfile);
        return "profile";

    }

    @RequestMapping(value = "/user/id/{id}")
    public String findUserByIdAndViewProfilePage(@PathVariable int id,
                                                 Model model) {
        return "redirect:/profile/" + userService.findOne(id).getUsername();
    }


    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String registrationPage(Model model) {

        model.addAttribute("newUser", new NewUserForm());
        return "signup";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String createUser(@Valid @ModelAttribute("newUser") NewUserForm newUser,
                             BindingResult result,
                             RedirectAttributes model) {

        if (result.hasErrors()) {
            return "signup";
        }

        User user = new User();
        user.setName(newUser.getName());
        user.setEmail(newUser.getEmail());
        user.setUsername(newUser.getUsername());
        user.setPassword(newUser.getPassword());

        userService.create(user);

        model.addFlashAttribute("message", "user.successfully.added");
        return "redirect:/login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String signin(Model model, String error, String logout) {

        if (error != null) {
            model.addAttribute("error", "Your username or password is invalid.");
        }

        if (logout != null) {
            model.addAttribute("message", "You have been logged out successfully.");
        }

        return "signin";
    }


    @RequestMapping(value = "/logout")
    public String logOutAndRedirectToSigninPage(Authentication authentication,
                                               HttpServletRequest request,
                                               HttpServletResponse response) {
        if (authentication != null) {
            new SecurityContextLogoutHandler().logout(request, response, authentication);
        }
        return "redirect:/login?logout=true";
    }


    @RequestMapping(value = "/profile")
    public String myProfile(Authentication authentication,
                            Model model) {
        String username = authentication.getName();
        UserProfile userProfile;

        try {
            userProfile = userProfileService.findOne(username);
        }

        catch (NullPointerException e) {
            throw new UserNotFoundException();
        }

        model.addAttribute("userProfile", userProfile);
        return "profile";
    }

}

package com.project.AdmoDb.controller.frontend;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class IndexController {

    @GetMapping({"/","/index"})
    public String homepage(@RequestParam(name = "home", required = false)
                                   String name, Model model) {
        model.addAttribute("home", name);
        return "index";
    }
}
